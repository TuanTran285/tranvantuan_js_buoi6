const kqDiem = document.querySelector("#button")


function domID(ID) {
    return document.getElementById(ID)
}


function tinhKhuVuc(khuVuc) {
    switch(khuVuc) {
        case "X": 
        return 0
        case "A": 
        return 2
        case "B":
        return 1
        case "C":
        return 0.5
    }
}

function tinhDoiTuong(doiTuong) {
    switch(doiTuong) {
        case 0: 
        return 0
        case 1:
        return 2.5
        case 2:
        return 1.5
        case 3:
        return 1
    }
}

function tinhDauHayRot() {
    const diemChuan = domID("diem-chuan").value * 1
    const khuVuc = domID("khu-vuc").value
    const doiTuong = domID("doi-tuong").value * 1
    const monThuNhat = domID("mon-thu-nhat").value * 1
    const monThuHai = domID("mon-thu-hai").value *1
    const monThuBa = domID("mon-thu-ba").value * 1
    const result = domID("result") 


    const diemKhuVuc = tinhKhuVuc(khuVuc)
    const diemDoiTuong = tinhDoiTuong(doiTuong)
    
    const tongDiem = monThuNhat + monThuHai + monThuBa + diemKhuVuc + diemDoiTuong
    if(monThuNhat <=0 || monThuHai<=0 || monThuBa <= 0) {
        result.innerText = `=> Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`
    }else {
        if(tongDiem >= diemChuan) {
            result.innerText = `=> Bạn đã đậu. Tổng điểm: ${tongDiem}`
        }else {
            result.innerText = `=> Bạn đã rớt. Tông điểm: ${tongDiem}`
        }
    }
}

kqDiem.addEventListener("click", function() {
    tinhDauHayRot()
})


// Tính tiền điện
// tạo biến sau này thay đổi giá điện fix dễ
const Dau_50_KW = 500
const KE_50_KW = 650
const KE_100_KW = 850
const KE_150_KW = 1100
const CONLAI = 1300

const tienDienEl = domID("tinh-tien-dien")

function tinhTienDien() {
    const tenEl = domID("nhap-ten").value
    const soKwEl = domID("nhap-so-kw").value * 1
    const result = domID("tong-tien-dien")
    let soTienDien
    if(soKwEl <=0) {
        confirm("Dữ liệu không hợp lệ")
    }else {
        if(soKwEl <= 50) {
            soTienDien = soKwEl * Dau_50_KW
        }else if(soKwEl <= 100) {
            soTienDien = Dau_50_KW * 50  + (soKwEl - 50) * KE_50_KW
        }else if(soKwEl <= 200) {
            soTienDien = Dau_50_KW * 50 + 50 * KE_50_KW + (soKwEl - 100) * KE_100_KW
        }else if(soKwEl <= 350) {
            soTienDien = Dau_50_KW * 50 + 50 * KE_50_KW + 100 * KE_100_KW + (soKwEl - 200) * KE_150_KW
            console.log(soTienDien)
        }else {
            soTienDien = Dau_50_KW * 50 + 50 * KE_50_KW + 100 * KE_100_KW + 150 * KE_150_KW + (soKwEl - 350) * CONLAI
        }
    }   
    result.innerHTML = `=> Họ tên: ${tenEl}; Tiền điện: ${soTienDien.toLocaleString()} VND`
}



tienDienEl.addEventListener("click", function() {
    tinhTienDien()
})

